//
//  DetailViewController.swift
//  SwiftUIRecipes
//
//  Created by Anton Iermilin on 24.07.2020.
//  Copyright © 2020 Anton Yermilin. All rights reserved.
//

import UIKit
import SwiftUI
import Combine

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.description
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }

    var detailItem: NSDate? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

