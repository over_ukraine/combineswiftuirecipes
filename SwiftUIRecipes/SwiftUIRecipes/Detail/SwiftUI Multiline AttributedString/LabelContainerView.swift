//
//  LabelContainerView.swift
//  SwiftUIRecipes
//
//  Created by Anton Iermilin on 29.07.2020.
//  Copyright © 2020 Anton Yermilin. All rights reserved.
//

import SwiftUI

struct AdaptiveStack<Content: View>: View
{
    @Environment(\.horizontalSizeClass) var sizeClass
    let horizontalAlignment: HorizontalAlignment
    let verticalAlignment: VerticalAlignment
    let spacing: CGFloat?
    let content: () -> Content

    init(horizontalAlignment: HorizontalAlignment = .center, verticalAlignment: VerticalAlignment = .center, spacing: CGFloat? = nil, @ViewBuilder content: @escaping () -> Content)
    {
        self.horizontalAlignment = horizontalAlignment
        self.verticalAlignment = verticalAlignment
        self.spacing = spacing
        self.content = content
    }

    var body: some View
    {
        Group
        {
            if sizeClass == .compact
            {
                VStack(alignment: horizontalAlignment, spacing: spacing, content: content)
            }
            else
            {
                HStack(alignment: verticalAlignment, spacing: spacing, content: content)
            }
        }
    }
}

struct TrialFeatureView: View
{
	@Environment(\.horizontalSizeClass) var sizeClass
	
	let iconName : String
	let iconNameCompact : String?
	let primaryText : String
	let secondaryText : String
	
    var body: some View
    {
		Group
		{
			if self.sizeClass == .compact
			{
				HStack(alignment: .top)
				{
					Image(systemName: self.iconNameCompact ?? self.iconName).colorInvert().colorMultiply(Color.red)
					
					GeometryReader
					{
						geometry in
						VStack(alignment: .leading, spacing: 3)
						{
							Text(LocalizedStringKey(self.primaryText), tableName: "Trial")
								.font(.subheadline)
								.fontWeight(.bold)
								.foregroundColor(.primary)
								.multilineTextAlignment(.leading)

							//Text(LocalizedStringKey(self.secondaryText), tableName: "Trial")
//								.font(.subheadline)
//								.multilineTextAlignment(.leading)
//								.foregroundColor(.secondary)
//								.fixedSize(horizontal: false, vertical: true)
						
							LabelView(availableWidth:geometry.size.width)
								.fixedSize(horizontal: false, vertical: true)
								.frame(width: geometry.size.width, alignment: .topLeading)
						}
					}.background(Color.green)
				}.fixedSize(horizontal: false, vertical: true)
			}
			else
			{
				VStack(alignment: .center)
				{
					Image(self.iconName).colorInvert().colorMultiply(Color.red)
					
					GeometryReader
					{
						geometry in
						VStack(alignment: .center, spacing: 3)
						{
							Text(LocalizedStringKey(self.primaryText), tableName: "Trial")
								.font(.subheadline)
								.fontWeight(.heavy)
								.multilineTextAlignment(.center)
								.fixedSize(horizontal: false, vertical: true)
							//Text(LocalizedStringKey(self.secondaryText), tableName: "Trial").font(.subheadline).multilineTextAlignment(.center).fixedSize(horizontal: false, vertical: true).opacity(0.6)
							
							LabelView(availableWidth:geometry.size.width)
								.fixedSize(horizontal: false, vertical: true)
						}
					}.background(Color.green)
				}
			}
		}
    }
}

struct TrialFeaturesView: View
{
    @Environment(\.horizontalSizeClass) var sizeClass

    var body: some View
    {
		AdaptiveStack(horizontalAlignment: .leading, verticalAlignment: .top, spacing: (.compact == sizeClass) ? 10 : 17)
		{
			TrialFeatureView(iconName: "person", iconNameCompact: nil, primaryText: "TrialFeatureTitle1", secondaryText: "TrialFeatureDescription1")
			TrialFeatureView(iconName: "person.2", iconNameCompact: nil, primaryText: "TrialFeatureTitle2", secondaryText: "TrialFeatureDescription2")
			TrialFeatureView(iconName: "person.3", iconNameCompact: nil, primaryText: "TrialFeatureTitle3", secondaryText: "TrialFeatureDescription3")
		}.padding([.top], (.compact == sizeClass) ? 15 : 40)
    }
}

struct LabelContainerView: View {
    var body: some View {
        TrialFeaturesView()
    }
}

struct LabelContainerView_Previews: PreviewProvider {
    static var previews: some View {
        LabelContainerView()
    }
}
