//
//  LabelHostingController.swift
//  SwiftUIRecipes
//
//  Created by Anton Iermilin on 29.07.2020.
//  Copyright © 2020 Anton Yermilin. All rights reserved.
//

import UIKit
import SwiftUI

class LabelHostingController: UIHostingController<LabelContainerView> {

    @objc required dynamic init?(coder aDecoder: NSCoder) {
        let rootView = LabelContainerView()
        
        super.init(coder: aDecoder, rootView: rootView)
    }
}
