//
//  LabelView.swift
//  SwiftUIRecipes
//
//  Created by Anton Iermilin on 29.07.2020.
//  Copyright © 2020 Anton Yermilin. All rights reserved.
//

import SwiftUI

struct LabelView: View, UIViewRepresentable {
    @State var availableWidth: CGFloat
    
    func makeUIView(context: Context) -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.attributedText = makeAttributedString()
        label.lineBreakMode = .byWordWrapping
        label.widthAnchor.constraint(equalToConstant: availableWidth).isActive = true
        return label
    }
    
    func updateUIView(_ uiView: UILabel, context: Context) {
        
        uiView.widthAnchor.constraint(equalToConstant: availableWidth).isActive = true
    }
    
    typealias UIViewType = UILabel
    
    func makeAttributedString() -> NSAttributedString {
        
        let attrString = NSMutableAttributedString(string: "What is love? Baby, don't hurt me")
        attrString.addAttributes( [.foregroundColor : UIColor.red], range: NSRange(location: 0, length: 12))
        attrString.addAttribute(.foregroundColor, value: UIColor.green, range: NSRange(location: 12, length: attrString.length - 12))
        return attrString
    }
}

struct LabelView_Previews: PreviewProvider {
    static var previews: some View {
        LabelView(availableWidth: 100)
    }
}
