//
//  SettingsView.swift
//  PublicControl
//
//  Created by Anton Iermilin on 29.06.2020.
//  Copyright © 2020 Anton Yermilin. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    @ObservedObject var viewModel: SettingsViewModel

//    @Environment(\.editMode) var editMode
//    @Binding var showAvatar: Bool
    
    init(model: SettingsViewModel = SettingsViewModel()) {
        viewModel = model
    }
    
    var body: some View {
        ZStack {
            Color(UIColor.systemBackground).edgesIgnoringSafeArea(.all)
            
            VStack {
                HStack {
//                    if showAvatar {
//                        Image(systemName: "photo").resizable().frame(width: 128, height: 128, alignment: .topLeading)
//                    }
                    VStack {
                        TextField("First Name", text: $viewModel.firstName).textFieldStyle(RoundedBorderTextFieldStyle())
                            .disabled(!viewModel.isEditing)
                        TextField("Last Name", text: $viewModel.lastName)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .disabled(!viewModel.isEditing)
                    }.padding()
                    .onAppear {
                            
                    }.onReceive(viewModel.$isEditing) { (isEditing) in
                        if isEditing == false {
                            self.viewModel.applyChanges()
                        }
                    }
                }.padding()
                Spacer(minLength: 0)
            }
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SettingsView(model: SettingsViewModel()).environment(\.colorScheme, .light)
            SettingsView(model: SettingsViewModel()).environment(\.colorScheme, .dark)
        }
    }
}
