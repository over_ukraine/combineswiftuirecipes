//
//  SettingsHostingViewController.swift
//  PublicControl
//
//  Created by Anton Iermilin on 01.07.2020.
//  Copyright © 2020 Anton Yermilin. All rights reserved.
//

import UIKit
import SwiftUI
import Combine

class SettingsHostingViewController: UIHostingController<SettingsView>, ObservableObject {
    let settingsView: SettingsView
    // FIXME: How to pass this as Binding to SettingsView directly? I want this persistant value to be my SoT
    var showAvatar = true

    @objc required dynamic init?(coder aDecoder: NSCoder) {
        settingsView = SettingsView()
        
        super.init(coder: aDecoder, rootView: settingsView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.rightBarButtonItem = editButtonItem
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        // FIXME: I want to update SettingsView @Environment \.editMode from this controller
        settingsView.viewModel.isEditing = editing
    }
}
