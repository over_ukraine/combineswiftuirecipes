//
//  SettingsViewModel.swift
//  PublicControl
//
//  Created by Anton Iermilin on 29.06.2020.
//  Copyright © 2020 Anton Yermilin. All rights reserved.
//

import Foundation
import Combine

class SettingsViewModel : NSObject, ObservableObject {

    @Published var isEditing: Bool = false
    
    @Published var firstName: String = ""
    @Published var lastName: String = ""
    
    func applyChanges() {
    }
    
}
