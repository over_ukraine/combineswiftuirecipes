---

# Purpose and Usage

Primary goal of this project is to give shared place for developers who studies Cobine / SwiftUI to model problem they deal with. 
Thus, all participants will have ability to pull changes and try to solve it in real environment. 

## Workflow
1. Main.storyboard intended to be used for transitions to specific case by clicking corresponding row in master table.
2. Make branch from master
3. Create separate storyboard or ViewController (hierarchy) and model the case there.
4. Mark problem places with FIXME followed by comment
5. Push to remote. 
6. Ask the question in group https://t.me/joinchat/GVipdRTcnIxA66LZJ9ylBA
7. Wait for solution to be pushed, or do it yourself.
8. Merge back to master


---

# Resources

Here are links to "must read" resources, will be updated.

## Combine

1. https://www.vadimbulavin.com/tag/combine/ – Brief and Simple Explanation of Combine concepts accompanied with examples. 
2. https://heckj.github.io/swiftui-notes/ – Combine Recipes/Reference collection. Complex English occures.

## SwiftUI

1. https://nalexn.github.io/stranger-things-swiftui-state/ – Brief overview of some non-obvious SwiftUI aspects

## Complex Solutions and Approaches

1. https://www.raywenderlich.com/4161005-mvvm-with-combine-tutorial-for-ios – MVVM with Combine Tutorial for iOS
2. https://www.vadimbulavin.com/modern-mvvm-ios-app-architecture-with-combine-and-swiftui/ –  MVVM with Combine and SwiftUI + Finite State Machine (CombineFeedback lib).
